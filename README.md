# gvc_resources

gvc_resources 是一个为[general version controller](https://github.com/moqsien/gvc)缓存github文件的地方。
众所周知，在中国大陆，正常访问github会受到GFW的限制，没有特殊工具，几乎无法下载到需要的软件或者源码，哪怕你获取的软件和源码不含任何违规内容。
而[gvc](https://github.com/moqsien/gvc)恰好使用了很多来自github的源码或者软件。
为了正常给大陆用户提供这些软件的更新，就有了gvc_resource这个仓库。


# gvc_resources缓存的软件来自哪里？

- [protobuf](https://github.com/protocolbuffers/protobuf)
- [vlang](https://github.com/vlang/v)
- [typst](https://github.com/typst/typst)
- [neovim](https://github.com/neovim/neovim)
- [vcpkg](https://github.com/microsoft/vcpkg)
- [vcpkg-tool](https://github.com/microsoft/vcpkg-tool)
- [pyenv](https://github.com/pyenv/pyenv)
- [pyenv-win](https://github.com/pyenv-win/pyenv-win)

所有软件或者脚本，通过[gscraper](https://github.com/moqsien/gscraper)自动获取并提交。
可以通过sha256验证。

# 提示

gvc和gvc_resources旨在提供学习和研究便利，请合法合理使用。
